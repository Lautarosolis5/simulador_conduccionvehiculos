using Mono.Cecil.Cil;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

namespace SC
{
    public class CarController2 : MonoBehaviour
    {
        private InputManager IM;
        private GameManager GM;

        [Header("Partes del Coche")]
        [SerializeField] WheelCollider[] wheelsColliders = new WheelCollider[4];
        [SerializeField] GameObject[] wheelMesh = new GameObject[4];
        [SerializeField] float[] slip = new float[4];
        [SerializeField] GameObject CenterOfMass;
        [SerializeField] DriveType Type;
        [SerializeField] BoxChange boxChange;

        [Header("Fisicas")]
        [SerializeField] float radius = 6;
        [SerializeField] float downForce = 50;
        [SerializeField] float Freno;
        [SerializeField] Rigidbody rb;

        [Header("Drift")]
        private WheelFrictionCurve forwardFriction;
        private WheelFrictionCurve sidewaysFriction;

        [Header("Sounds")]
        [HideInInspector] public bool test;

        [Header("Variables")]
        public float handBrakeFrictionMultiplier = 2f;  
        public float engineRPM;
        public float KMH;
        public bool playPauseSmoke = false;
        public float horizontal;
        public float vertical;
        public float totalPower;
        public float[] gears;
        public int gearNum = 1;
        public AnimationCurve enginePower;

        // variables Privadas
        private float maxRPM, minRPM;
        private float wheelsRPM;
        private float smoothTime = 0.09f;
        private float tempo;
        private float driftFactor;

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
            IM = GetComponent<InputManager>();
            GM = FindObjectOfType<GameManager>();

            rb.centerOfMass = CenterOfMass.transform.localPosition;
            StartCoroutine(timedLoop());
        }

        private void Update()
        {
            horizontal = IM.horizontal;
            vertical = IM.vertical;

            calculateEnginePower();
            DownForce();
            MoveY();
            MoveH(horizontal);
            AnimatedWheels();
            GetFriction();
            Drift();
        }

        private void MoveY()
        {

            if (Type == DriveType.TodasLasRuedaass)
            {
                for (int i = 0; i < wheelsColliders.Length; i++)
                {
                    wheelsColliders[i].motorTorque = vertical * totalPower / 4;

                }
            }
            else if (Type == DriveType.RuedasTraseras)
            {
                for (int i = 2; i < wheelsColliders.Length; i++)
                {
                    wheelsColliders[i].motorTorque = vertical * totalPower / 2;

                }
            }
            else if (Type == DriveType.RuedasDelanteras)
            {
                for (int i = 0; i < wheelsColliders.Length - 2; i++)
                {
                    wheelsColliders[i].motorTorque = vertical * totalPower / 2;

                }
            }

            KMH = rb.velocity.magnitude * 3.6f;

        }

        private void MoveH(float horizontal)
        {
            if (horizontal > 0)
            {
                wheelsColliders[0].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius + (1.5f / 2))) * horizontal;
                wheelsColliders[1].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius - (1.5f / 2))) * horizontal;
            }
            else if (horizontal < 0)
            {
                wheelsColliders[0].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius - (1.5f / 2))) * horizontal;
                wheelsColliders[1].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius + (1.5f / 2))) * horizontal;
            }
            else
            {
                wheelsColliders[0].steerAngle = 0;
                wheelsColliders[1].steerAngle = 0;
            }
        }

        private void AnimatedWheels()
        {
            Vector3 wheelPosition = Vector3.zero;
            Quaternion wheelRotation = Quaternion.identity;

            for (int i = 0; i < 4; i++)
            {
                wheelsColliders[i].GetWorldPose(out wheelPosition, out wheelRotation);
                wheelMesh[i].transform.position = wheelPosition;
                wheelMesh[i].transform.rotation = wheelRotation;
            }
        }

        private void DownForce()
        {
            rb.AddForce(-transform.up * downForce * rb.velocity.magnitude);
        }

        private void GetFriction()
        {
            for (int i = 0; i < wheelsColliders.Length; i++)
            {
                WheelHit wheelHit;
                wheelsColliders[i].GetGroundHit(out wheelHit);

                slip[i] = wheelHit.forwardSlip;
            }
        }

        private void wheelRPM()
        {
            float sum = 0;
            int R = 0;
            for (int i = 0; i < 4; i++)
            {
                sum += wheelsColliders[i].rpm;
                R++;
            }
            wheelsRPM = (R != 0) ? sum / R : 0;

        }

        private void calculateEnginePower()
        {

            wheelRPM();

            totalPower = enginePower.Evaluate(engineRPM) * (gears[gearNum]) * vertical;
            float velocity = 0.0f;
            engineRPM = Mathf.SmoothDamp(engineRPM, 1000 + (Mathf.Abs(wheelsRPM) * 3.6f * gears[gearNum]), ref velocity, smoothTime);

            MoveY();
            shifter();
        }

        private void shifter()
        {
            // Cambio Automatico
            if(boxChange == BoxChange.automatic)
            {
                if(engineRPM > maxRPM && gearNum < gears.Length - 1)
                {
                    gearNum++;
                    GM.changeGear();
                }

            } // Cambio Manual 
            else if(boxChange == BoxChange.manual)
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    gearNum++;
                    GM.changeGear();
                }
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    gearNum--;
                    GM.changeGear();
                }
            }

            if (engineRPM < minRPM && gearNum > 0)
            {
                gearNum--;
                GM.changeGear();
            }
        }

        private bool isGrounded()
        {
            if (wheelsColliders[0].isGrounded && wheelsColliders[1].isGrounded && wheelsColliders[2].isGrounded && wheelsColliders[3].isGrounded)
                return true;
            else
                return false;
        }

        private void Drift()
        {
            //tine it takes to go from normal drive to drift 
            float driftSmothFactor = .7f * Time.deltaTime;

            if (IM.handBrake)
            {
                sidewaysFriction = wheelsColliders[0].sidewaysFriction;
                forwardFriction = wheelsColliders[0].forwardFriction;

                float velocity = 0;
                sidewaysFriction.extremumValue = sidewaysFriction.asymptoteValue = forwardFriction.extremumValue = forwardFriction.asymptoteValue =
                    Mathf.SmoothDamp(forwardFriction.asymptoteValue, driftFactor * handBrakeFrictionMultiplier, ref velocity, driftSmothFactor);

                for (int i = 0; i < 4; i++)
                {
                    wheelsColliders[i].sidewaysFriction = sidewaysFriction;
                    wheelsColliders[i].forwardFriction = forwardFriction;
                }

                sidewaysFriction.extremumValue = sidewaysFriction.asymptoteValue = forwardFriction.extremumValue = forwardFriction.asymptoteValue = 1.1f;
                //extra grip for the front wheels
                for (int i = 0; i < 2; i++)
                {
                    wheelsColliders[i].sidewaysFriction = sidewaysFriction;
                    wheelsColliders[i].forwardFriction = forwardFriction;
                }
                rb.AddForce(transform.forward * (KMH / 400) * 10000);
            }
            //executed when handbrake is being held
            else
            {

                forwardFriction = wheelsColliders[0].forwardFriction;
                sidewaysFriction = wheelsColliders[0].sidewaysFriction;

                forwardFriction.extremumValue = forwardFriction.asymptoteValue = sidewaysFriction.extremumValue = sidewaysFriction.asymptoteValue =
                    ((KMH * handBrakeFrictionMultiplier) / 300) + 1;

                for (int i = 0; i < 4; i++)
                {
                    wheelsColliders[i].forwardFriction = forwardFriction;
                    wheelsColliders[i].sidewaysFriction = sidewaysFriction;

                }
            }

            //checks the amount of slip to control the drift
            for (int i = 2; i < 4; i++)
            {

                WheelHit wheelHit;

                wheelsColliders[i].GetGroundHit(out wheelHit);
                //smoke
                if (wheelHit.sidewaysSlip >= 0.3f || wheelHit.sidewaysSlip <= -0.3f || wheelHit.forwardSlip >= .3f || wheelHit.forwardSlip <= -0.3f)
                    playPauseSmoke = true;
                else
                    playPauseSmoke = false;


                if (wheelHit.sidewaysSlip < 0) driftFactor = (1 + -IM.horizontal) * Mathf.Abs(wheelHit.sidewaysSlip);

                if (wheelHit.sidewaysSlip > 0) driftFactor = (1 + IM.horizontal) * Mathf.Abs(wheelHit.sidewaysSlip);
            }

        }

        private IEnumerator timedLoop()
        {
            while (true)
            {
                yield return new WaitForSeconds(.7f);
                radius = 6 + KMH / 20;

            }
        }

    }
}
