using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SC
{
    public class CarEffects : MonoBehaviour
    {
        [SerializeField] CarController2 carController;
        public ParticleSystem[] smoke;

        private void Awake()
        {
            carController = GetComponent<CarController2>();
        }

        private void Update()
        {
            if (carController.playPauseSmoke)
            {
                startSmoke();
            }
            else
            {
                StopSmoke();
            }
        }

        public void startSmoke()
        {
            for (int i = 0; i < smoke.Length; i++)
            {
                smoke[i].Play();
            }
        }

        public void StopSmoke()
        {
            for (int i = 0; i < smoke.Length; i++)
            {
                smoke[i].Stop();
            }
        }
    }
}
