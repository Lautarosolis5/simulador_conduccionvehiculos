using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SC
{
    public class CamaraController : MonoBehaviour
    {
        [SerializeField] float moveSmoothness;
        [SerializeField] float rotSmoothness;

        [SerializeField] Vector3 moveOffset;
        [SerializeField] Vector3 rotOffset;

        public Transform carTarget;

        private void FixedUpdate()
        {
            cameraFollow();
            cameraRotation();
        }

        private void cameraFollow()
        {
            Vector3 targetPos = new Vector3();
            targetPos = carTarget.TransformPoint(moveOffset);

            transform.position = Vector3.Lerp(transform.position, targetPos, moveSmoothness * Time.deltaTime);
        }

        private void cameraRotation()
        {
            var direction= carTarget.position - transform.position;
            var rotation = new Quaternion();

            rotation = Quaternion.LookRotation(direction + rotOffset, Vector3.up);

            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, rotSmoothness * Time.deltaTime);
        }
    }
}
