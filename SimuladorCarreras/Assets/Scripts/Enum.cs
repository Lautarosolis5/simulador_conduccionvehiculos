using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SC
{
    public class Enum : MonoBehaviour
    {

    }

    public enum Axle
    {
        front,
        back
    }

    public enum DriveType
    {
        RuedasDelanteras,
        RuedasTraseras,
        TodasLasRuedaass
    }

    public enum BoxChange
    {
        automatic,
        manual
    }
}
