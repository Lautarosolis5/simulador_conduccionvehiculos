using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace SC
{
    public class GameManager : MonoBehaviour
    {
        [Header("Medidor de Velocidad")]
        public GameObject Aguja;
        [SerializeField] float startPosition, endPosition;
        [SerializeField] float desiredPosition;
        [SerializeField] TextMeshProUGUI KHM;
        [SerializeField] TextMeshProUGUI GearNum;
        [SerializeField] CarController2 carController;

        private void Awake()
        {
            carController = FindObjectOfType<CarController2>();
        }
        private void FixedUpdate()
        {
            KHM.text = carController.KMH.ToString("0");
            updateAguja();
        }

        public void updateAguja()
        {
            desiredPosition = startPosition - endPosition;
            float temp = carController.engineRPM / 10000;
            Aguja.transform.eulerAngles = new Vector3(0, 0, (startPosition - temp * desiredPosition));
        }

        public void changeGear()
        {
            GearNum.text = carController.gearNum.ToString();
        }
    }
}
